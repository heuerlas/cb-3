use crate::C1Token::{
    And, Assign, Asterisk, ConstBoolean, ConstFloat, ConstInt, Equal, Greater, GreaterEqual,
    Identifier, KwBoolean, KwFloat, KwIf, KwInt, KwPrintf, KwReturn, KwVoid, LeftBrace,
    LeftParenthesis, Less, LessEqual, Minus, NotEqual, Or, Plus, RightBrace, RightParenthesis,
    Semicolon, Slash,
};
use crate::{C1Lexer, C1Token, ParseResult};

pub struct C1Parser<'a> {
    lexer: C1Lexer<'a>,
}
const EXPR: [C1Token; 6] = [Equal, NotEqual, LessEqual, GreaterEqual, Less, Greater];
const SIMPEXPR: [C1Token; 3] = [Plus, Minus, Or];
const TERM: [C1Token; 3] = [Asterisk, Slash, And];

impl C1Parser<'_> {
    pub fn parse(text: &str) -> ParseResult {
        C1Parser {
            lexer: C1Lexer::new(text),
        }
        .program()
    }
    fn program(&mut self) -> ParseResult {
        while self.lexer.current_token() != None {
            self.functiondefinition()?;
        }
        Ok(())
    }
    fn functiondefinition(&mut self) -> ParseResult {
        self.type_match()?;
        self.eat_and_check(Some(Identifier))?;
        self.eat_and_check(Some(LeftParenthesis))?;
        self.eat_and_check(Some(RightParenthesis))?;
        self.eat_and_check(Some(LeftBrace))?;
        self.statementlist()?;
        self.eat_and_check(Some(RightBrace))
    }
    fn type_match(&mut self) -> ParseResult {
        return match self.lexer.current_token() {
            Some(KwBoolean | KwFloat | KwInt | KwVoid) => {
                self.lexer.eat();
                Ok(())
            }
            _ => Err(format!(
                "Error in line: {} expected type got {:?}",
                self.lexer.current_line_number().unwrap(),
                self.lexer.current_token().unwrap()
            )),
        };
    }
    fn statementlist(&mut self) -> ParseResult {
        while self.lexer.current_token() != Some(RightBrace) {
            self.block()?;
        }
        Ok(())
    }
    fn eat_and_check(&mut self, token: Option<C1Token>) -> ParseResult {
        if self.lexer.current_token() == token {
            self.lexer.eat();
            return Ok(());
        }
        return Err(format!(
            "Error in line: {} got {:?}",
            self.lexer.current_line_number().unwrap(),
            self.lexer.current_token().unwrap()
        ));
    }
    fn block(&mut self) -> ParseResult {
        if self.lexer.current_token() == Some(LeftBrace) {
            self.lexer.eat();
            self.statementlist()?;
            self.eat_and_check(Some(RightBrace))
        } else {
            self.statement()
        }
    }
    fn statement(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(KwIf) => self.ifstatement(),
            Some(KwReturn) => {
                self.returnstatement()?;
                self.eat_and_check(Some(Semicolon))
            }
            Some(KwPrintf) => {
                self.printstatement()?;
                self.eat_and_check(Some(Semicolon))
            }
            Some(Identifier) => {
                if self.lexer.peek_token() == Some(Assign) {
                    self.statassignment()?;
                    return self.eat_and_check(Some(Semicolon));
                } else if self.lexer.peek_token() == Some(LeftParenthesis) {
                    self.functioncall()?;
                    return self.eat_and_check(Some(Semicolon));
                }
                Err(format!(
                    "Error in line: {} expected statement got {:?}",
                    self.lexer.current_line_number().unwrap(),
                    self.lexer.current_token().unwrap()
                ))
            }
            _ => Err(format!(
                "Error in line: {} expected statement got {:?}",
                self.lexer.current_line_number().unwrap(),
                self.lexer.current_token().unwrap()
            )),
        }
    }

    fn ifstatement(&mut self) -> ParseResult {
        self.eat_and_check(Some(KwIf))?;
        self.eat_and_check(Some(LeftParenthesis))?;
        self.assignment()?;
        self.eat_and_check(Some(RightParenthesis))?;
        self.block()
    }
    fn returnstatement(&mut self) -> ParseResult {
        self.eat_and_check(Some(KwReturn))?;
        if self.lexer.current_token() != Some(Semicolon) {
            self.assignment()?;
        }
        Ok(())
    }
    fn printstatement(&mut self) -> ParseResult {
        self.eat_and_check(Some(KwPrintf))?;
        self.eat_and_check(Some(LeftParenthesis))?;
        self.assignment()?;
        self.eat_and_check(Some(RightParenthesis))
    }
    fn statassignment(&mut self) -> ParseResult {
        self.eat_and_check(Some(Identifier))?;
        self.eat_and_check(Some(Assign))?;
        self.assignment()
    }
    fn functioncall(&mut self) -> ParseResult {
        self.eat_and_check(Some(Identifier))?;
        self.eat_and_check(Some(LeftParenthesis))?;
        self.eat_and_check(Some(RightParenthesis))
    }
    fn assignment(&mut self) -> ParseResult {
        if self.lexer.peek_token() == Some(Assign) {
            self.lexer.eat();
            self.eat_and_check(Some(Assign))?;
            self.assignment()
        } else {
            self.expr()
        }
    }
    fn expr(&mut self) -> ParseResult {
        self.simpexpr()?;
        while EXPR.contains(&self.lexer.current_token().unwrap()) {
            self.lexer.eat();
            self.simpexpr()?;
        }
        Ok(())
    }
    fn simpexpr(&mut self) -> ParseResult {
        if self.lexer.current_token() == Some(Minus) {
            self.lexer.eat();
        }
        self.term()?;
        while SIMPEXPR.contains(&self.lexer.current_token().unwrap()) {
            self.lexer.eat();
            self.term()?;
        }
        Ok(())
    }
    fn term(&mut self) -> ParseResult {
        self.factor()?;
        while TERM.contains(&self.lexer.current_token().unwrap()) {
            self.lexer.eat();
            self.factor()?;
        }
        Ok(())
    }
    fn factor(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(ConstInt) | Some(ConstFloat) | Some(ConstBoolean) => {
                self.lexer.eat();
                Ok(())
            }
            Some(Identifier) => {
                if self.lexer.peek_token() == Some(LeftParenthesis) {
                    return self.functioncall();
                }
                self.lexer.eat();
                Ok(())
            }
            Some(LeftParenthesis) => {
                self.lexer.eat();
                self.assignment()?;
                self.eat_and_check(Some(RightParenthesis))
            }
            _ => Err(format!(
                "Error in line: {} expecting factor got {:?}",
                self.lexer.current_line_number().unwrap(),
                self.lexer.current_token().unwrap()
            )),
        }
    }
}
